<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests\PostFormRequest;

class PostsController extends Controller
{
    public function readAll()
    {
        // $posts = Post::all();
        $posts = Post::paginate(7); // a page post
        return view("home", compact("posts"));
    }
    public function create()
    {
        return view('create');
    }
    public function viewpost($id)
    {
        $post = Post::find($id);

        return view('post')->with('post', $post);
    }

    public function addPost(PostFormRequest $request)
    {
        $title = $request->input('title');
        $description = $request->input('description');
        $content = $request->input('content');

        Post::create([
            'title' => $title,
            'description' => $description,
            'content' => $content
        ]);
        return redirect()->route('home');
    }
    // call form edit
    public function editpost($id)
    {
        $post = Post::find($id);
        return view("edit", compact("post"));
    }
    // update post to db
    public function updatepost($id, PostFormRequest $request)
    {
        $post = Post::find($id);
        $post->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'content' => $request->get('content')
        ]);
        return redirect()->route('home');
    }
    public function deletePost($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect()->route('home');
    }

    // api controller


}
