<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;

class Postcontroller extends Controller
{

    public function index()
    {
        $posts = Post::all();
        return json_encode($posts);
    }


    public function create()
    {
        return "create";
    }

    public function store(Request $request)
    {
        // insert all data to database : POST METHOD
        $post = Post::create($request->all());
        return response()->json($post, 201);
    }

    public function show($id)
    {
        // return info 1 post GET METHOD
        $post = Post::find($id);
        return json_encode($post);
    }

    public function search($name)
    {
        $posts = Post::query()
            ->where('title', 'LIKE', "%$name%")
            ->get();
        return json_encode($posts);
    }


    public function edit($id)
    {
        return "edit";
        //
    }

    public function update(Request $request, $id)
    {
        // update to post: PUT method
        $post = Post::findOrFail($id);
        $post->update($request->all());
        return $post;
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return 204;
    }
}
