<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class HomeController extends Controller
{
    public function index()
    {
        return 'Home Page!';
        $posts = Post::all();
        // dd($posts);
        // return view("home",compact("posts"));
        return view("home", compact("posts"));
    }
    public function homes()
    {
        return view("homes");
    }
}
