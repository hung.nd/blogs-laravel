@extends('app.master')

@section('title', $post->title)

@section('content')
<div class="container">
  <h1>{{$post->title}}</h1>
  <p>{{$post->content}}</p>
  <p><a href="{{url('/')}}">back</a> | <a href="{{route('post.edit',$post->id)}}">EDIT</a>
  <form action="{{ route('post.delete', $post->id) }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="submit" value="Delete">
  </form>
  </p>
</div>


@stop