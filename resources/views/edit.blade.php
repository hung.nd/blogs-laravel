@extends('app.master')

@section('title', 'EDIT Page')

@section('content')
<style>
    input[type=text],
    select {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    input[type=submit] {
        width: 100%;
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    input[type=submit]:hover {
        background-color: #45a049;
    }

    div {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
    }
</style>


<h3>UPDATE POST</h3>

<div>
    @if(count($errors)>0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input <br>
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{ route('edit.update', $post->id) }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for="title">Title</label>
        <input type="text" id="title" name="title" required placeholder="Your title.." value ="{{$post->title}}">
        <label for="description">Description</label>
        <input type="text" id="description" name="description" required placeholder="Your description.."  value ="{{ $post->description }}">
        <div><label for="content">Content</label></div>
        <textarea rows="10" cols="100%" type="text" id="content" name="content" required placeholder="Your last content.."> {{$post->content}}</textarea>
        <input type="submit" value="Update">
    </form>
</div>

@stop