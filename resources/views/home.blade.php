@extends('app.master')

@section('title', 'HOME Page')

@section('content')
<div class="container">
  @foreach ($posts as $post)
  <div class="row">
    <div clas="col-sm-6 col-sm-offset-3">
      <h2>{{ $post->title }}</h2>
      <p>{{ $post->description }}</p>
      <p><a href="{{route('post.viewpost',[$post->id,$post->slug])}}">Readmore</a> | <a href="{{route('post.edit',$post->id)}}">Update</a></p>
    </div>
  </div>
  @endforeach
  <div class="row">
    {{ $posts ->render() }}
  </div>
</div>
@stop