@extends('app.master')

@section('title', 'Create Page')

@section('content')
<style>
    input[type=text],
    select {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    input[type=submit] {
        width: 100%;
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    input[type=submit]:hover {
        background-color: #45a049;
    }

    div {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
    }
</style>


<h3>INSERT TO DB</h3>

<div>
    @if(count($errors)>0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input <br>
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{ route('post.addpost') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for="title">Title</label>
        <input type="text" id="title" name="title" required placeholder="Your title..">

        <label for="description">Description</label>
        <input type="text" id="description" name="description" required placeholder="Your description..">

        <label for="content">Content</label>
        <textarea rows="4" cols="100%" type="text" id="content" name="content" required placeholder="Your last content.."></textarea>


        <input type="submit" value="Submit">
    </form>
</div>

@stop