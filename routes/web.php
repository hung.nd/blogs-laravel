<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// home page
Route::get('/', [
  'as' => 'home',
  'uses' => 'PostsController@readAll'
]);

// create post page
Route::get('/post/create', [
  'as' => 'post.create',
  'uses' => 'PostsController@create'
]);

//EX insert post
Route::post('/post', [
  'as' => 'post.addpost',
  'uses' => 'PostsController@addPost'
]);

// form edit post
Route::get('/post/{id}/edit', [
  'as' => 'post.edit',
  'uses' => 'PostsController@editpost'
]);


// view a post
Route::get('/post/{id}/{slug}', [
  'as' => 'post.viewpost',
  'uses' => 'PostsController@viewpost'
]);

//EX edit post
Route::post('/edit/{id}', [
  'as' => 'edit.update',
  'uses' => 'PostsController@updatepost'
]);

// delete a post
Route::post('/post/{id}', [
  'as' => 'post.delete',
  'uses' => 'PostsController@deletePost'
]);
